SELECT SUM(tre.quilometragem) AS total_percorrido 
FROM aluno al
JOIN mesociclo mes ON  al.id=mes.id_aluno
JOIN microciclo mic ON mes.id=mic.id_mesociclo
JOIN treino tre ON tre.id_microciclo=mic.id
WHERE nome = 'Ana' AND data_treino >= '2019-08-01' AND data_treino <= '2019-08-31'
