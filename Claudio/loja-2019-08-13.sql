-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.3.16-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.2.0.5668
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela loja.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(50) DEFAULT NULL,
  `ICONE` varchar(50) DEFAULT NULL,
  `DESCRICAO` text DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela loja.categoria: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`ID`, `NOME`, `ICONE`, `DESCRICAO`) VALUES
	(1, 'CELULAR', '', ''),
	(2, 'GAMES', '', ''),
	(3, 'COMPUTADOR', '', '');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;

-- Copiando estrutura para tabela loja.compras
CREATE TABLE IF NOT EXISTS `compras` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PRODUTOS` int(11) DEFAULT 0,
  `ID_USUARIO` int(11) DEFAULT 0,
  `DATA_COMPRA` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela loja.compras: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;
INSERT INTO `compras` (`ID`, `ID_PRODUTOS`, `ID_USUARIO`, `DATA_COMPRA`) VALUES
	(1, 1, 1, '2019-08-06'),
	(2, 1, 2, '2019-08-06'),
	(3, 1, 3, '2019-08-06'),
	(4, 2, 2, '2019-08-05'),
	(5, 3, 1, '2019-08-04');
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;

-- Copiando estrutura para tabela loja.produtos
CREATE TABLE IF NOT EXISTS `produtos` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(50) DEFAULT NULL,
  `PRECO` decimal(10,2) DEFAULT NULL,
  `IMAGEM` varchar(50) DEFAULT NULL,
  `DESCRICAO` text DEFAULT NULL,
  `ID_CATEGORIA` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela loja.produtos: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT INTO `produtos` (`ID`, `NOME`, `PRECO`, `IMAGEM`, `DESCRICAO`, `ID_CATEGORIA`) VALUES
	(1, 'SAMSUNG GALAXY A8', 3449.00, NULL, NULL, '1'),
	(2, 'MOTO G5 PLUS', 569.00, NULL, NULL, '1'),
	(3, 'LG K11 PLUS', 629.00, NULL, NULL, '1'),
	(4, 'I-PHONE 8 64GB', 2549.00, NULL, NULL, '1'),
	(5, 'PLAYSTATION 4 SLIM', 2199.99, NULL, NULL, '2'),
	(6, 'X-BOX ONE', 2369.99, NULL, NULL, '2'),
	(7, 'PLAYSTATION 3 32GB', 1399.99, NULL, NULL, '2'),
	(8, 'LEGACY INTEL', 934.69, NULL, NULL, '3'),
	(9, 'MOTION INTEL CORE', 2384.99, NULL, NULL, '3'),
	(10, 'SAMSUNG ESSENCIALS', 1699.99, NULL, NULL, '3');
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;

-- Copiando estrutura para tabela loja.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN` varchar(50) DEFAULT NULL,
  `SENHA` varchar(50) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `NOME` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela loja.usuarios: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`ID`, `LOGIN`, `SENHA`, `EMAIL`, `NOME`) VALUES
	(1, 'CLAUDINHOFDM', 'ABAB1213', 'CLAUDINHOFDM@GMAIL.COM', 'CLAUDIO FARIAS'),
	(2, 'JOAOPONTI', 'ABAB1213', 'JOAOPONTI@GMAIL.COM', 'JOAO PONTI'),
	(3, 'ANDREGUAZZELI', 'ABAB1213', 'ANDREGUAZZELI@GMAIL.COM', 'ANDRE GUAZZELI');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
